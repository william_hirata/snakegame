Project created with Unity2018.1.5f1
When running, please select portrait ratios. Still works in other ratios but will look a little strange.

You can configure the game in BalanceData, it's a scriptableObject that changes there will reflect into gameplay.

The project was made taking into account expanding the features.

To play the game, use the arrows to start.

By William K. Hirata