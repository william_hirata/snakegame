﻿
using System.Text;

public class SupportMethods {

    public static void PrintGridAsMatrix(GridPosition[] grid, GridConfig gridConfig)
    {
        StringBuilder stringBuilder = new StringBuilder();

        int gridTotalRows = gridConfig.rows;
        int gridTotalCols = gridConfig.cols;
        
        for (int row = gridTotalRows - 1; row >= 0 ; row--)
        {
            for (int col = 0; col < gridTotalCols; col++)
            {
                stringBuilder.Append(((int)grid[SupportMethods.GetGridIndexByRowAndCol(row, col, gridConfig)].IDInPosition).ToString());
            }
            stringBuilder.Append("\n");
        }
        UnityEngine.Debug.Log(stringBuilder.ToString());
    }

    public static int GetGridIndexByRowAndCol(int rowPosition, int colPosition, GridConfig gridConfig)
    {
        return rowPosition * gridConfig.cols + colPosition;
    }

    public static bool IsOpposite(SnakeMovementEnum a, SnakeMovementEnum b)
    {
        switch(a)
        {
            case SnakeMovementEnum.UP:
                return b == SnakeMovementEnum.DOWN;
            case SnakeMovementEnum.RIGHT:
                return b == SnakeMovementEnum.LEFT;
            case SnakeMovementEnum.DOWN:
                return b == SnakeMovementEnum.UP;
            case SnakeMovementEnum.LEFT:
                return b == SnakeMovementEnum.RIGHT;
            default:
                return false;
        }
    }
}
