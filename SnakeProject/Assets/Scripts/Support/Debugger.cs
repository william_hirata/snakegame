﻿
public class Debugger {

    public static string DebugFormat(object obj, string errorDescription)
    {
        return string.Format("{0}: {1}", obj.GetType().Name, errorDescription);
    }
}
