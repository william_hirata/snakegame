﻿using UnityEngine;

[CreateAssetMenu(fileName = "BalanceData", menuName = "Config/Game", order = 1)]
public class BalanceScriptableObject : ScriptableObject {
    public int rowsInGrid;
    public int colsInGrid;
    public int tilesPerSecond;
    public int initialFruitAmount;
    public int snakeIncreasePerFruit;
    public int pointsPerFruit;
}
