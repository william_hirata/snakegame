﻿using UnityEngine;

public class BalanceConstants {

    private static BalanceScriptableObject balanceData = Resources.Load<BalanceScriptableObject>("BalanceData");


    
    public static int GetGridInitialFruitAmount()
    {
        return balanceData.initialFruitAmount;
    }

    public static int GetSnakeIncreasePerFruit()
    {
        return balanceData.snakeIncreasePerFruit;
    }

    public static int GetPointsPerFruit()
    {
        return balanceData.pointsPerFruit;
    }

    public static GridConfig GetGridConfig()
    {
        return new GridConfig
        {
            rows = balanceData.rowsInGrid,
            cols = balanceData.colsInGrid,
            tilesPerSecond = balanceData.tilesPerSecond
        };
    }
}
