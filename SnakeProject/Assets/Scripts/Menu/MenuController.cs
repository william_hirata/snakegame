﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

    [SerializeField]
    private InputField playerName;
    [SerializeField]
    private Transform highScores;
    [SerializeField]
    private GameObject scoreCellPrefab;



    public void Play()
    {
        PlayerPrefs.SetString(GameConstants.PLAYERPREFS_PLAYERNAME_ID, playerName.text);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
    }

    void Awake()
    {
        List<GameHighScore> scores = GameHighScore.GetHighScores();
        int totalScores = scores.Count;
        List<GameHighScore> orderedScores = scores.OrderByDescending(x => x.points).ToList();

        int index = 0;
        foreach(GameHighScore scoreIt in orderedScores)
        {
            GameObject.Instantiate(scoreCellPrefab, highScores, false).GetComponent<HighScoreCellController>().Setup(index + 1, scoreIt);
            index++;
        }
    }
}
