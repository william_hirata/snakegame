﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreCellController : MonoBehaviour {

    [SerializeField]
    private Text label;


    
    public void Setup(int index, GameHighScore highScore)
    {
        label.text = string.Format("{0} - {1} - {2} {3}", index.ToString(), highScore.points, highScore.playerName, highScore.won ? "- Win" : "");
    }
}
