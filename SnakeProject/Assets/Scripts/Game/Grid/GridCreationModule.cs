﻿
public class GridCreationModule
{
    public GameGrid CreateInitialGrid(GridConfig gridConfig)
    {
        GridPosition[] createdGrid = new GridPosition[gridConfig.GetGridSize()];
        GridPosition createdPosition = null;

        int maxRowValue = gridConfig.rows - 1;
        int maxColValue = gridConfig.cols - 1;

        for(int rowAux = 0; rowAux <= maxRowValue; rowAux++)
        {
            for(int colAux = 0; colAux <= maxColValue; colAux++)
            {
                if(rowAux == 0 || rowAux == maxRowValue || colAux == 0 || colAux == maxColValue)
                {
                    createdPosition = new GridPosition(rowAux, colAux, gridConfig, GridIDsEnum.WALL);
                }
                else
                {
                    createdPosition = new GridPosition(rowAux, colAux, gridConfig);
                }

                createdGrid[createdPosition.index] = createdPosition;
            }
        } 

        return new GameGrid
        {
            gridConfig = gridConfig,
            gridPositions = createdGrid
        };
    }
}
