﻿using NUnit.Framework;

[TestFixture]
public class GridTests
{
    private GridConfig gridConfig;

    [SetUp]
    public void Init()
    {
        gridConfig = new GridConfig();
        gridConfig.rows = 3;
        gridConfig.cols = 5;
    }

    #region GridConfigTests

    [Test]
    public void TestGridConfigSize()
    {
        Assert.AreEqual(gridConfig.GetGridSize(), 15);
    }

    #endregion

    #region GridPositionTests

    [Test]
    public void TestGridPositionByMatrixValue()
    {
        GridPosition gridPosition = new GridPosition(2, 3, gridConfig);
        
        Assert.AreEqual(2, gridPosition.row);
        Assert.AreEqual(3, gridPosition.col);
        Assert.AreEqual(13, gridPosition.index);
        Assert.AreEqual(GridIDsEnum.NONE, gridPosition.IDInPosition);
    }

    [Test]
    public void TestGridPositionByMatrixValueAndID()
    {
        GridPosition gridPosition = new GridPosition(1, 4, gridConfig, GridIDsEnum.WALL);
        
        Assert.AreEqual(1, gridPosition.row, 1);
        Assert.AreEqual(4, gridPosition.col, 4);
        Assert.AreEqual(9, gridPosition.index, 9);
        Assert.AreEqual(GridIDsEnum.WALL, gridPosition.IDInPosition);
    }

    [Test]
    public void TestGridPositionByIndexValue()
    {
        GridPosition gridPosition = new GridPosition(7, gridConfig);
        
        Assert.AreEqual(1, gridPosition.row);
        Assert.AreEqual(2, gridPosition.col);
        Assert.AreEqual(7, gridPosition.index);
        Assert.AreEqual(GridIDsEnum.NONE, gridPosition.IDInPosition);
    }

    [Test]
    public void TestGridPositionByIndexValueAndID()
    {
        GridPosition gridPosition = new GridPosition(2, gridConfig, GridIDsEnum.SNAKE);
        
        Assert.AreEqual(0, gridPosition.row);
        Assert.AreEqual(2, gridPosition.col);
        Assert.AreEqual(2, gridPosition.index);
        Assert.AreEqual(GridIDsEnum.SNAKE, gridPosition.IDInPosition);
    }

    #endregion

    #region GridModule

    [Test]
    public void TestGridModule_CreateInitialGrid()
    {
        GridCreationModule gridModule = new GridCreationModule();
        GameGrid gameGrid = gridModule.CreateInitialGrid(gridConfig);

        int gridConfigMaxRow = gridConfig.rows - 1;
        int gridConfigMaxCol = gridConfig.cols - 1;
        foreach(GridPosition gridIt in gameGrid.gridPositions)
        {
            if(gridIt.row == 0 || gridIt.col == 0 || gridIt.row == gridConfigMaxRow || gridIt.col == gridConfigMaxCol)
            {
                Assert.AreEqual(GridIDsEnum.WALL, gridIt.IDInPosition, string.Format("Row: {0}\nCol: {1}", gridIt.row, gridIt.col));
            }
            else
            {
                Assert.AreEqual(GridIDsEnum.NONE, gridIt.IDInPosition, string.Format("Row: {0}\nCol: {1}", gridIt.row, gridIt.col));
            }
        }
    }

    #endregion
}
