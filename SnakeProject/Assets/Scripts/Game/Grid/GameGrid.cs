﻿
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class GameGrid {
    public GridConfig gridConfig;
    public GridPosition[] gridPositions;

    

    public void PopulateSnakePositions(GameSnake gameSnake)
    {
        foreach(GridPosition positionIt in gameSnake.positions)
        {
            gridPositions[positionIt.index].SetGridID(positionIt.IDInPosition);
        }
    }

    public bool CheckWinGame()
    {
        IEnumerable<GridPosition> availablePositions = gridPositions.Where(x => x.IDInPosition == GridIDsEnum.NONE || x.IDInPosition == GridIDsEnum.FRUIT);
        //TODO refactor to use snake size with grid size
        return availablePositions.Count() <= 0;
    }

    public GameFruitAddResult PopulateFruit(int quantity)
    {
        List<GridPosition> fruitPositions = new List<GridPosition>();
        while(quantity > 0)
        {
            IEnumerable<GridPosition> availablePositions = gridPositions.Where(x => x.IDInPosition == GridIDsEnum.NONE);
            if(availablePositions.Count() <= 0)
            {
                break;
            }
            int selectedIndex = availablePositions.ElementAt(Random.Range(0, availablePositions.Count())).index;
            gridPositions[selectedIndex].SetGridID(GridIDsEnum.FRUIT);
            fruitPositions.Add(gridPositions[selectedIndex]);
            quantity--;
        }

        return new GameFruitAddResult
        {
            addedGridPositions = fruitPositions
        };
    }

    public GameFruitRemoveResult RemoveFruit(GridPosition gridPosition)
    {
        if(gridPositions[gridPosition.index].IDInPosition == GridIDsEnum.FRUIT)
        {
            gridPositions[gridPosition.index].SetGridID(GridIDsEnum.NONE);
        }

        return new GameFruitRemoveResult
        {
            removedPosition = gridPosition
        };
    }

    public void UpdateGridPositionsBySnakeMovement(GameSnakeMovementResult movementResult)
    {
        if(movementResult.addGridPosition != null)
        {
            gridPositions[movementResult.addGridPosition.index].SetGridID(GridIDsEnum.SNAKE);
        }

        if(movementResult.removeGridPosition != null)
        {
            gridPositions[movementResult.removeGridPosition.index].SetGridID(GridIDsEnum.NONE);
        }
    }

    public GridIDsEnum GetIDByMovement(GridPosition gridPosition, SnakeMovementEnum movementDirection)
    {
        GameMovementConfig gameMovementConfig = new GameMovementConfig(gridPosition, movementDirection);

        return gridPositions[SupportMethods.GetGridIndexByRowAndCol(gameMovementConfig.rowAfterMovement, gameMovementConfig.colAfterMovement, gridConfig)].IDInPosition;
    }

    public GridIDsEnum GetIDInPosition(GridPosition gridPosition)
    {
        return gridPositions[gridPosition.index].IDInPosition;
    }
}
