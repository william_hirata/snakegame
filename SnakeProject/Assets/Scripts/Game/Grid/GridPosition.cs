﻿using UnityEngine.Assertions;

public class GridPosition
{
    public int row {get; private set;}
    public int col {get; private set;}
    public int index {get; private set;}
    public GridIDsEnum IDInPosition {get; private set;}



    public GridPosition(int rowPosition, int colPosition, GridConfig gridConfig, GridIDsEnum gridIDenum) : this(rowPosition, colPosition, gridConfig)
    {
        SetGridID(gridIDenum);
    }

    public GridPosition(int rowPosition, int colPosition, GridConfig gridConfig)
    {
        SetValuesByMatrixPosition(rowPosition, colPosition, gridConfig);
    }

    public GridPosition(int indexPosition, GridConfig gridConfig, GridIDsEnum gridIDenum) : this(indexPosition, gridConfig)
    {
        SetGridID(gridIDenum);
    }

    public GridPosition(int indexPosition, GridConfig gridConfig)
    {
        SetValuesByIndexPosition(indexPosition, gridConfig);
    }

    public void SetValuesByMatrixPosition(int rowPosition, int colPosition, GridConfig gridConfig)
    {
        row = rowPosition;
        col = colPosition;
        index = SupportMethods.GetGridIndexByRowAndCol(row, col, gridConfig);

        Assert.IsTrue(row < gridConfig.rows, Debugger.DebugFormat(this, "row value greater than config rows"));
        Assert.IsTrue(col < gridConfig.cols, Debugger.DebugFormat(this, "col value greater than config cols"));
    }

    public void SetValuesByIndexPosition(int indexPosition, GridConfig gridConfig)
    {
        index = indexPosition;
        row = UnityEngine.Mathf.FloorToInt(index/gridConfig.cols);
        col = indexPosition % gridConfig.cols;

        Assert.IsTrue(row < gridConfig.rows, Debugger.DebugFormat(this, "row value greater than config rows"));
        Assert.IsTrue(col < gridConfig.cols, Debugger.DebugFormat(this, "col value greater than config cols"));
    }

    public void SetGridID(GridIDsEnum gridIDenum)
    {
        IDInPosition = gridIDenum;
    }
}
