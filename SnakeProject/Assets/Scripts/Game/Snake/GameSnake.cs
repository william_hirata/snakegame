﻿using System.Collections.Generic;

public class GameSnake {
    private int totalPoints;
    public GridConfig gridConfig {get; private set;}
    public List<GridPosition> positions {get; private set;}
    public int partsQueued {get; private set;}
    public SnakeMovementEnum nextDirection {get; private set;}
    public SnakeMovementEnum lastDirection {get; private set;}

    public GameSnakeMovementResult movementResult = new GameSnakeMovementResult();



    public GameSnake(){}

    public GameSnake(GridConfig gridConfig, List<GridPosition> positions)
    {
        this.gridConfig = gridConfig;
        this.positions = positions;
    }

    public bool HasPartsQueued()
    {
        return partsQueued > 0;
    }

    public GameSnakeMovementResult MoveBy(SnakeMovementEnum movement)
    {
        movementResult.Clear();
        GameMovementConfig gameMovement = new GameMovementConfig(positions[0], movement);

        DoMovement(gameMovement, ref movementResult);
        return movementResult;
    }

    public GameSnakeMovementResult Move()
    {
        movementResult.Clear();
        
        GameMovementConfig gameMovement = new GameMovementConfig(positions[0], nextDirection);
        lastDirection = nextDirection;

        DoMovement(gameMovement, ref movementResult);
        return movementResult;
    }

    public int GetTotalPoints()
    {
        return totalPoints;
    }

    public void SetDirection(SnakeMovementEnum movementDirection)
    {
        if(SupportMethods.IsOpposite(movementDirection, lastDirection))
        {
            return ;
        }

        nextDirection = movementDirection;
    }

    public void DoMovement(GameMovementConfig gameMovement, ref GameSnakeMovementResult movementResult)
    {
        if(HasPartsQueued())
        {
            partsQueued--;
            AddNewHeadGridPosition(gameMovement, ref movementResult);
        }
        else
        {
            MoveTailGridPosition(gameMovement, ref movementResult);
        }
    }

    public void AddPartsQueue()
    {
        totalPoints+= BalanceConstants.GetPointsPerFruit();
        partsQueued+= BalanceConstants.GetSnakeIncreasePerFruit();
    }

    public GridPosition GetHeadPosition()
    {
        return positions[0];
    }

    private void AddNewHeadGridPosition(GameMovementConfig gameMovement, ref GameSnakeMovementResult movementResult)
    {
        GridPosition newHeadGridPosition = new GridPosition(gameMovement.rowAfterMovement, gameMovement.colAfterMovement, gridConfig, GridIDsEnum.SNAKE);
        positions.Insert(0, newHeadGridPosition);
        movementResult.addGridPosition = new GridPosition(newHeadGridPosition.index, gridConfig, GridIDsEnum.SNAKE);
    }

    private void MoveTailGridPosition(GameMovementConfig gameMovement, ref GameSnakeMovementResult movementResult)
    {
        GridPosition tailGridPosition = positions[positions.Count - 1];
        GridPosition removedGridPosition = new GridPosition(tailGridPosition.index, gridConfig);

        tailGridPosition.SetValuesByMatrixPosition(gameMovement.rowAfterMovement, gameMovement.colAfterMovement, gridConfig);
        positions.Remove(tailGridPosition);
        positions.Insert(0, tailGridPosition);

        movementResult.removeGridPosition = removedGridPosition;
        movementResult.addGridPosition = new GridPosition(tailGridPosition.index, gridConfig, GridIDsEnum.SNAKE);
    }
}
