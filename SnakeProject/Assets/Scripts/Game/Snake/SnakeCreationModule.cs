﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SnakeCreationModule {
    
    public GameSnake CreateInitialSnake(GameGrid gameGrid)
    {
        IEnumerable<GridPosition> availablePositions = gameGrid.gridPositions.Where(x => x.IDInPosition == GridIDsEnum.NONE);
        return new GameSnake(gameGrid.gridConfig, new List<GridPosition>() {
            new GridPosition(availablePositions.ElementAt(Random.Range(0, availablePositions.Count())).index, gameGrid.gridConfig, GridIDsEnum.SNAKE
        )});
    }
}
