﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TopHUDController : MonoBehaviour {

    [SerializeField]
    private Text pointsLabel;
    [SerializeField]
    private GameObject pausePrefab;

    private GameFlowEvent flowEvents;

    

    public void UpdateHUD(GameHUDEventConfig hudUpdate)
    {
        pointsLabel.text = string.Format("Points: {0}", hudUpdate.totalPoints);
    }

    public void RegisterFlowEvents(GameController gameController)
    {
        flowEvents.AddListener(gameController.UpdateGame);
    }

    public void PauseGame()
    {
        flowEvents.Invoke(new GameFlowEventConfig{
            pause = true
        });
        GameObject rootCanvas = GameObject.Find("RootCanvas");
        GameObject.Instantiate(pausePrefab, rootCanvas.transform, false).GetComponent<PauseController>().Setup(flowEvents);
    }

    void Awake()
    {
        flowEvents = new GameFlowEvent();
    }
}
