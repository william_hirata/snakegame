﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameController : MonoBehaviour {

    [SerializeField]
    private Text resultLabel;
    [SerializeField]
    private Text pointsLabel;
    


    public void Setup(bool didWon, int points)
    {
        resultLabel.text = didWon ? @"YOU WON!" : @"YOU LOSE";
        pointsLabel.text = string.Format(@"TOTAL: {0} Points", points);
    }

    public void BackToMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(GameConstants.MENU_SCENE_NAME);
    }
}
