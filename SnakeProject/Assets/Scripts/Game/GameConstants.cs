﻿
public class GameConstants
{
    public const string MENU_SCENE_NAME = "Menu";
    public const string GAME_SCENE_NAME = "Game";

    public const string PLAYERPREFS_PLAYERNAME_ID = "playerName";
    public const string PLAYERPREFS_SCORES_ID = "scores";
}

public enum GridIDsEnum
{
    NONE = 0,
    WALL = 1,
    FRUIT = 2,
    SNAKE = 3
}

public enum SnakeMovementEnum
{
    NONE = 0,
    INVALID = 1,
    UP = 2,
    RIGHT = 3,
    DOWN = 4,
    LEFT = 5
}
