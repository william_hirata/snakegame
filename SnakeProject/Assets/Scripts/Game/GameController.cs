﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour {

    [SerializeField]
    private GameObject endGamePrefab;
    [SerializeField]
    private GameGraphicsController gameGraphics;

    private GridConfig gridConfig;
    private GameGrid gameGrid;
    private GameSnake gameSnake;
    private SnakeMovementEnum chosenMovement;
    private GameHUDEvent hudEvents;
    private bool hasTutorial;


    public void UpdateGame(GameFlowEventConfig flowEventConfig)
    {
        Time.timeScale = flowEventConfig.pause ? 0 : 1;
    }

    void Setup(GridConfig gridConfigToCreate = null)
    {
        if(gridConfigToCreate == null)
        {
            gridConfig = BalanceConstants.GetGridConfig();
        }
        else
        {
            gridConfig = gridConfigToCreate;
        }

        GridCreationModule gridCreationModule = new GridCreationModule();
        gameGrid = gridCreationModule.CreateInitialGrid(gridConfig);
        SnakeCreationModule snakeCreationModule = new SnakeCreationModule();
        gameSnake = snakeCreationModule.CreateInitialSnake(gameGrid);
        gameGrid.PopulateSnakePositions(gameSnake);
        PopulateFruit(BalanceConstants.GetGridInitialFruitAmount());

        gameGraphics.Setup(gameGrid.gridConfig, gameGrid.gridPositions);

        hasTutorial = true;
    }

    private GameFruitAddResult PopulateFruit(int amount = -1)
    {
        GameFruitAddResult fruitAddResult = gameGrid.PopulateFruit(amount > 0 ? amount : 1);

        return fruitAddResult;
    }

    private GameFruitRemoveResult EatFruit(GameSnakeMovementResult gridPosition)
    {
        GameFruitRemoveResult fruitRemoveResult = gameGrid.RemoveFruit(gridPosition.addGridPosition);

        return fruitRemoveResult;
    }

    private IEnumerator MovementTick()
    {
        WaitForSeconds tickTime = new WaitForSeconds(1f/(float)gridConfig.tilesPerSecond);
        bool gameIsRunning = true;
        bool wonGame = false;
        while(gameIsRunning)
        {
            yield return tickTime;

            if(gameSnake.nextDirection == SnakeMovementEnum.NONE)
            {
                continue;
            }

            GridIDsEnum nextTileID = gameGrid.GetIDByMovement(gameSnake.GetHeadPosition(), gameSnake.nextDirection);
            if(nextTileID == GridIDsEnum.FRUIT)
            {
                gameSnake.AddPartsQueue();
            }

            GameSnakeMovementResult movementResult = gameSnake.Move();
            GameFruitAddResult fruitAddResult = null;
            GameFruitRemoveResult fruitEatResult = null;

            gameGrid.UpdateGridPositionsBySnakeMovement(movementResult);
            
            switch(nextTileID)
            {
                case GridIDsEnum.WALL:
                case GridIDsEnum.SNAKE:
                default:
                    gameIsRunning = false;
                    continue;
                case GridIDsEnum.NONE:
                    break;
                case GridIDsEnum.FRUIT:
                    fruitEatResult = EatFruit(movementResult);
                    fruitAddResult = PopulateFruit();
                    break;             
            }
            
            hudEvents.Invoke(new GameHUDEventConfig{
                totalPoints = gameSnake.GetTotalPoints()
            });
            gameGraphics.UpdateTiles(movementResult, fruitAddResult, fruitEatResult);
            if(gameGrid.CheckWinGame())
            {
                gameIsRunning = false;
                wonGame = true;
            }
        }

        GameHighScore.AddNewHighScore(PlayerPrefs.GetString(GameConstants.PLAYERPREFS_PLAYERNAME_ID, ""), gameSnake.GetTotalPoints(), wonGame);

        //TODO refactor to use animator for game state
        GameObject rootCanvas = GameObject.Find("RootCanvas");
        GameObject.Instantiate(endGamePrefab, rootCanvas.transform, false).GetComponent<EndGameController>().Setup(wonGame, gameSnake.GetTotalPoints());
    }

    public void SetDirection(SnakeMovementEnum movementDirection)
    {
        //When implementing touch controls, use this method
        
        if(hasTutorial)
        {
            GameObject tutorialObject = GameObject.Find("WaitStart");
            Destroy(tutorialObject);
            hasTutorial = false;
        }

        gameSnake.SetDirection(movementDirection);
    }

    void Start () {
        hudEvents = new GameHUDEvent();

        TopHUDController topHudController = GameObject.Find("TopHUD").GetComponent<TopHUDController>();

        hudEvents.AddListener(topHudController.UpdateHUD);
        topHudController.RegisterFlowEvents(this);
        Setup();
        StartCoroutine(MovementTick());
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            SetDirection(SnakeMovementEnum.UP);
        } 
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            SetDirection(SnakeMovementEnum.RIGHT);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            SetDirection(SnakeMovementEnum.DOWN);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            SetDirection(SnakeMovementEnum.LEFT);
        }
    }
}
