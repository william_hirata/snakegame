﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour {

    private GameFlowEvent flowEvents;

    

    public void Setup(GameFlowEvent gameFlowEvents)
    {
        flowEvents = gameFlowEvents;
    }

    public void BackToGame()
    {
        flowEvents.Invoke(new GameFlowEventConfig{
            pause = false
        });
        Destroy(gameObject);
    }
}
