﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GameGraphicsController : MonoBehaviour {

    [SerializeField]
    private Transform floorTransform;
    [SerializeField]
    private GameObject wallPrefab;
    [SerializeField]
    private GameObject snakePrefab;
    [SerializeField]
    private GameObject fruitPrefab;
    [SerializeField]
    private Transform gridElementsTransform;

    private int maxSide;
    private float tileSize;
    private float halfTileSize;
    private float initialPosition;
    private float initialRowPosition;
    private float initialColPosition;
    private GameObject[] objectInTiles;

    

    public void Setup(GridConfig gridConfig, GridPosition[] gridPositions)
    {
        PopulateInitialTiles(gridConfig, gridPositions);
    }

    public void UpdateTiles(GameSnakeMovementResult movementResult, GameFruitAddResult fruitAddResult, GameFruitRemoveResult fruitRemoveResult)
    {
        if(movementResult.removeGridPosition != null)
        {
            RemoveTile(movementResult.removeGridPosition);
        }

        if(fruitRemoveResult != null)
        {
            RemoveTile(fruitRemoveResult.removedPosition);
        }

        if(movementResult.addGridPosition != null)
        {
            CreateTile(movementResult.addGridPosition);
        }

        if(fruitAddResult != null)
        {
            foreach(GridPosition gridPositionIt in fruitAddResult.addedGridPositions)
            {
                CreateTile(gridPositionIt);
            }
        }
    }

    private void CreateTile(GridPosition gridPosition)
    {
        Assert.IsNotNull(gridPosition);
        GameObject instantiatedObject = null;
        switch(gridPosition.IDInPosition)
        {
            case GridIDsEnum.WALL:
                instantiatedObject = GameObject.Instantiate(wallPrefab, gridElementsTransform, false);
                break;
            case GridIDsEnum.SNAKE:
                instantiatedObject = GameObject.Instantiate(snakePrefab, gridElementsTransform, false);
                break;
            case GridIDsEnum.FRUIT:
                instantiatedObject = GameObject.Instantiate(fruitPrefab, gridElementsTransform, false);
                break;
            default:
                return;
        }
        instantiatedObject.transform.localPosition = new Vector3(initialColPosition + gridPosition.col * tileSize, initialRowPosition + gridPosition.row * tileSize, 0f);
        instantiatedObject.transform.localScale = new Vector3(tileSize, tileSize, tileSize);
        objectInTiles[gridPosition.index] = instantiatedObject;
    }

    private void RemoveTile(GridPosition gridPosition)
    {
        Assert.IsNotNull(gridPosition);
        Destroy(objectInTiles[gridPosition.index]);
        objectInTiles[gridPosition.index] = null;
    }

    private void PopulateInitialTiles(GridConfig gridConfig, GridPosition[] gridPositions)
    {
        objectInTiles = new GameObject[gridPositions.Length];
        maxSide = Mathf.Max(gridConfig.rows, gridConfig.cols);
        tileSize = 10f/maxSide;
        halfTileSize = tileSize/2f;
        initialPosition = -5f + halfTileSize;
        initialRowPosition = initialPosition + (maxSide - gridConfig.rows) * halfTileSize;
        initialColPosition = initialPosition + (maxSide - gridConfig.cols) * halfTileSize;

        if(gridConfig.rows != gridConfig.cols)
        {
            if(gridConfig.rows > gridConfig.cols)
            {
                floorTransform.localScale = new Vector3(10 * (float)gridConfig.cols / (float)gridConfig.rows, 10f, 1f);
            }
            else
            {
                floorTransform.localScale = new Vector3(10f, 10 * (float)gridConfig.rows / (float)gridConfig.cols, 1f);
            }
        }
        

        foreach(GridPosition gridPositionIt in gridPositions)
        {
            CreateTile(gridPositionIt);
        }
    }
}
