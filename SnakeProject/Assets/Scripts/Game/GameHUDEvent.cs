﻿using UnityEngine.Events;

public class GameHUDEvent : UnityEvent<GameHUDEventConfig>{}
public class GameHUDEventConfig
{
    public int totalPoints;
}