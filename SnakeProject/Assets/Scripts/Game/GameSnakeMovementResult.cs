﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSnakeMovementResult
{
    public GridPosition removeGridPosition;
    public GridPosition addGridPosition;

    

    public void Clear()
    {
        removeGridPosition = null;
        addGridPosition = null;
    }
}
