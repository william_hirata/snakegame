﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMovementConfig {
    public int row {get; private set;}
    public int col {get; private set;}
    public int rowAfterMovement {get; private set;}
    public int colAfterMovement {get; private set;}
    public SnakeMovementEnum movement {get; private set;}

    

    public GameMovementConfig(GridPosition gridPosition, SnakeMovementEnum movementDirection) : this(gridPosition.row, gridPosition.col, movementDirection){}

    public GameMovementConfig(int rowPosition, int colPosition, SnakeMovementEnum movementDirection)
    {
        row = rowPosition;
        col = colPosition;
        movement = movementDirection;
        UpdateValues();
    }

    public void SetMovement(SnakeMovementEnum movementDirection)
    {
        movement = movementDirection;
        UpdateValues();
    }

    private void UpdateValues()
    {
        rowAfterMovement = GameMovementConfig.GetRowByMovement(row, movement);
        colAfterMovement = GameMovementConfig.GetColByMovement(col, movement);
    }

    public static int GetRowByMovement(int rowPosition, SnakeMovementEnum movementDirection)
    {
        switch(movementDirection)
        {
            case SnakeMovementEnum.UP:
                return rowPosition + 1;
            case SnakeMovementEnum.RIGHT:
                return rowPosition;
            case SnakeMovementEnum.DOWN:
                return rowPosition -1;
            case SnakeMovementEnum.LEFT:
                return rowPosition;
            default:
                return rowPosition;
        }
    }

    public static int GetColByMovement(int colPosition, SnakeMovementEnum movementDirection)
    {
        switch(movementDirection)
        {
            case SnakeMovementEnum.UP:
                return colPosition;
            case SnakeMovementEnum.RIGHT:
                return colPosition + 1;
            case SnakeMovementEnum.DOWN:
                return colPosition;
            case SnakeMovementEnum.LEFT:
                return colPosition - 1;
            default:
                return colPosition;
        }
    }
}
