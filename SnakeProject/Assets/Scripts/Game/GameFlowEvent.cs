﻿using UnityEngine.Events;

public class GameFlowEvent : UnityEvent<GameFlowEventConfig>{}
public class GameFlowEventConfig
{
    public bool pause;
}