﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFruitRemoveResult
{
    public GridPosition removedPosition;

    public void Clear()
    {
        removedPosition = null;
    }
}

public class GameFruitAddResult
{
    public List<GridPosition> addedGridPositions;

    public GameFruitAddResult()
    {
        addedGridPositions = new List<GridPosition>();
    }

    public void Clear()
    {
        addedGridPositions.Clear();
    }
}
