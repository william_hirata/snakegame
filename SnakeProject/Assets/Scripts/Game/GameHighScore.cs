﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[Serializable]
public class GameHighScore
{
    public string playerName;
    public int points;
    public bool won;



    public static List<GameHighScore> GetHighScores()
    {
        GameHighScore[] scoresArray = JsonHelper.FromJson<GameHighScore>(PlayerPrefs.GetString(GameConstants.PLAYERPREFS_SCORES_ID, ""));
        if(scoresArray == null)
        {
            return new List<GameHighScore>();
        }

        List<GameHighScore> scores = scoresArray.ToList();
        if(scores == null)
        {
            scores = new List<GameHighScore>();
        }

        return scores;
    }

    public static void AddNewHighScore(string addedPlayerName, int addedPoints, bool addedWon)
    {
        List<GameHighScore> highScores = GetHighScores();
        highScores.Add(new GameHighScore
        {
            playerName = addedPlayerName,
            points = addedPoints,
            won = addedWon
        });

        PlayerPrefs.SetString(GameConstants.PLAYERPREFS_SCORES_ID, JsonHelper.ToJson<GameHighScore>(highScores.ToArray(), false));
    }
}
